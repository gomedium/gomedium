package tui

import (
	"fmt"
	"gitlab.com/gomedium/gomedium/tui/keys"
	"gitlab.com/gomedium/gomedium/tui/styles"
	"io"
	"time"

	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
)

type itemDelegate struct {
	keys   *keys.KeyMap
	styles *styles.Styles
}

func newItemDelegate(keys *keys.KeyMap, styles *styles.Styles) *itemDelegate {
	return &itemDelegate{
		keys:   keys,
		styles: styles,
	}
}

func (d itemDelegate) Height() int                               { return 1 }
func (d itemDelegate) Spacing() int                              { return 0 }
func (d itemDelegate) Update(msg tea.Msg, m *list.Model) tea.Cmd { return nil }

func HumanDate(timestamp int64) string {
	t := time.Unix(timestamp/1000, 0)
	return t.Format("Mon, 02 Jan 2006 15:04:05")
}
func (d itemDelegate) Render(w io.Writer, m list.Model, index int, listItem list.Item) {
	i, ok := listItem.(article)
	if !ok {
		return
	}

	title := d.styles.NormalTitle.Render
	desc := d.styles.NormalDesc.Render

	if index == m.Index() {
		title = func(s string) string {
			return d.styles.SelectedTitle.Render("> " + s)
		}
		desc = func(s string) string {
			return d.styles.SelectedDesc.Render(s)
		}
	}

	articleTitle := title(i.Title)
	byline := desc(i.Subtitle)
	itemListStyle := fmt.Sprintf("%s %s\n%s\n", articleTitle, desc(HumanDate(i.Timestamp)), byline)

	fmt.Fprint(w, itemListStyle)
}

func (d itemDelegate) ShortHelp() []key.Binding {
	return []key.Binding{}
}

func (d itemDelegate) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{d.keys.Back, d.keys.Quit, d.keys.ForceQuit},
	}
}
