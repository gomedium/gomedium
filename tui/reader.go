package tui

import (
	"fmt"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/gomedium/gomedium/core"
	"gitlab.com/gomedium/gomedium/tui/styles"
	"strings"
)

const useHighPerformanceRenderer = false

var (
	headerStyle = func() lipgloss.Style {
		b := lipgloss.RoundedBorder()
		b.Right = "├"
		return lipgloss.NewStyle().BorderStyle(b).Padding(0, 1)
	}()
	//metaStyle = func() lipgloss.Style {
	//
	//}()
	infoStyle = func() lipgloss.Style {
		b := lipgloss.RoundedBorder()
		b.Left = "┤"
		return headerStyle.Copy().BorderStyle(b)
	}()
)

type reader struct {
	post     core.Post
	ready    bool
	viewport viewport.Model
	styles   *styles.Styles
}

func NewReader() *reader {
	return &reader{}
}

func readerUpdate(msg tea.Msg, m Model) (tea.Model, tea.Cmd) {
	var (
		cmd  tea.Cmd
		cmds []tea.Cmd
	)

	switch msg := msg.(type) {
	case tea.KeyMsg:
		if k := msg.String(); k == "ctrl+c" || k == "q" || k == "esc" || k == "backspace" {
			m.state = PubView
			m.updateKeyBindings()
			return m, cmd
		}

	case tea.WindowSizeMsg:
		headerHeight := lipgloss.Height(m.headerView())
		metaHeight := lipgloss.Height(m.metaView())
		footerHeight := lipgloss.Height(m.footerView())
		verticalMarginHeight := headerHeight + metaHeight + footerHeight

		if !m.reader.ready {
			// Since this program is using the full size of the viewport we
			// need to wait until we've received the window dimensions before
			// we can initialize the viewport. The initial dimensions come in
			// quickly, though asynchronously, which is why we wait for them
			// here.
			m.reader.viewport = viewport.New(msg.Width, msg.Height-verticalMarginHeight)
			m.reader.viewport.YPosition = headerHeight + metaHeight + 4
			m.reader.viewport.HighPerformanceRendering = useHighPerformanceRenderer
			if len(m.reader.post.ID) > 0 {
				m.reader.viewport.SetContent(m.reader.post.Markdown())
			}
			m.reader.ready = true

			// This is only necessary for high performance rendering, which in
			// most cases you won't need.
			//
			// Render the viewport one line below the header.
			m.reader.viewport.YPosition = headerHeight + metaHeight + 4
		} else {
			m.reader.viewport.Width = msg.Width
			m.reader.viewport.Height = msg.Height - verticalMarginHeight
		}

		if useHighPerformanceRenderer {
			// Render (or re-render) the whole viewport. Necessary both to
			// initialize the viewport and when the window is resized.
			//
			// This is needed for high-performance rendering only.
			cmds = append(cmds, viewport.Sync(m.reader.viewport))
		}
	}

	// Handle keyboard and mouse events in the viewport
	m.reader.viewport, cmd = m.reader.viewport.Update(msg)
	cmds = append(cmds, cmd)

	return m, tea.Batch(cmds...)
}

func (m Model) readerView() string {
	if !m.reader.ready {
		return "\n  Initializing..."
	}
	return fmt.Sprintf("%s\n%s\n%s\n%s", m.headerView(), "", m.reader.viewport.View(), m.footerView())
}

func (m Model) metaView() string {
	return ""
}

func (m Model) headerView() string {

	info := fmt.Sprintf("[%s] [~%1.f minutes] [%d claps]\n", HumanDate(m.reader.post.Timestamp), m.reader.post.ReadingTime, m.reader.post.ClapCount)
	for _, t := range m.reader.post.Tags {
		info = info + "#" + t + " "
	}

	title := headerStyle.Render(m.styles.Header.Render(m.reader.post.Title) + "\n" + info)

	line := strings.Repeat("─", max(0, m.reader.viewport.Width-lipgloss.Width(title)))
	return lipgloss.JoinHorizontal(lipgloss.Center, title, line)
}

func (m Model) footerView() string {
	info := infoStyle.Render(fmt.Sprintf("%3.f%%", m.reader.viewport.ScrollPercent()*100))
	line := strings.Repeat("─", max(0, m.reader.viewport.Width-lipgloss.Width(info)))
	return lipgloss.JoinHorizontal(lipgloss.Center, line, info)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
