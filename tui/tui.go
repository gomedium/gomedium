package tui

import (
	"context"
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/glamour"
	"gitlab.com/gomedium/gomedium/core"
	"gitlab.com/gomedium/gomedium/tui/keys"
	"gitlab.com/gomedium/gomedium/tui/styles"
)

type UIState string

const (
	InitState   UIState = "init"
	ArticleView UIState = "article"
	HomeView    UIState = "home"
	PubView     UIState = "publication"
)

const (
	defaultWidth = 40
	listHeight   = 20
)

type article core.Post

func (a article) FilterValue() string { return a.ID }

type Model struct {
	state UIState
	//cursor      int
	//currentPost core.Post
	posts  list.Model
	styles styles.Styles
	keys   *keys.KeyMap
	reader *reader
}

func NewModel() Model {
	pub, err := core.PublicationHome(context.TODO(), "https://survivingtomorrow.org")
	if err != nil {
		panic(err)
	}
	posts, _ := core.PublicationPosts(context.TODO(), pub)
	var items []list.Item
	for _, p := range posts {
		items = append(items, article{
			ID:          p.ID,
			Publication: p.Publication,
			Title:       p.Title,
			Subtitle:    p.Subtitle,
			URL:         p.URL,
			Timestamp:   p.Timestamp,
			ClapCount:   p.ClapCount,
			ReadingTime: p.ReadingTime,
			Tags:        p.Tags,
			Content:     p.Content,
		})
	}
	styles := styles.DefaultStyles()
	keys := keys.NewKeyMap()

	l := list.New(items, newItemDelegate(keys, &styles), defaultWidth, listHeight)
	l.Title = pub.Name
	l.SetShowStatusBar(false)
	l.Styles.PaginationStyle = styles.Pagination
	l.Styles.HelpStyle = styles.Help
	return Model{
		state:  PubView,
		posts:  l,
		styles: styles,
		keys:   keys,
		reader: NewReader(),
	}
}

func (m *Model) updateKeyBindings() {
	if m.posts.SettingFilter() {
		m.keys.Enter.SetEnabled(false)
	}

	switch m.state {
	case PubView:
		m.keys.Back.SetEnabled(false)
	case ArticleView:
		m.keys.Back.SetEnabled(true)
		m.keys.Enter.SetEnabled(false)
		m.posts.KeyMap.AcceptWhileFiltering.SetEnabled(false)
		m.posts.KeyMap.CancelWhileFiltering.SetEnabled(false)
	}
}

func (m Model) Init() tea.Cmd {
	return nil
}

func listUpdate(msg tea.Msg, m Model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.posts.SetWidth(msg.Width)
		_, cmd := readerUpdate(msg, m)
		return m, cmd
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.posts.KeyMap.AcceptWhileFiltering):
			m.state = PubView
			m.updateKeyBindings()
		case key.Matches(msg, m.keys.CursorUp):
			m.posts.CursorUp()
		case key.Matches(msg, m.keys.CursorDown):
			m.posts.CursorDown()
		case key.Matches(msg, m.keys.Enter):
			if i, ok := m.posts.SelectedItem().(article); ok {
				m.reader.post = core.Post{
					ID:          i.ID,
					Title:       i.Title,
					Subtitle:    i.Subtitle,
					Content:     i.Content,
					Publication: i.Publication,
					URL:         i.URL,
					ReadingTime: i.ReadingTime,
					Tags:        i.Tags,
					ClapCount:   i.ClapCount,
					Timestamp:   i.Timestamp,
					Topic:       i.Topic,
				}
				m.state = ArticleView
				content, err := glamour.Render(m.reader.post.Markdown(), "dark")
				if err == nil {
					m.reader.viewport.SetContent(content)
				} else {
					m.reader.viewport.SetContent(m.reader.post.Markdown())
				}
			}
		}
	}
	var (
		cmds []tea.Cmd
		cmd  tea.Cmd
	)
	m.posts, cmd = m.posts.Update(msg)
	cmds = append(cmds, cmd)

	return m, tea.Batch(cmds...)
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	if m.posts.SettingFilter() {
		m.keys.Enter.SetEnabled(false)
	}

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, m.keys.Quit):
			return m, tea.Quit

		case key.Matches(msg, m.keys.ForceQuit):
			return m, tea.Quit
		}
	}
	switch m.state {
	case PubView:
		return listUpdate(msg, m)
	case ArticleView:
		return readerUpdate(msg, m)
	default:
		return m, nil

	}
}

func (m Model) View() string {
	switch m.state {
	case PubView:
		return "\n" + m.posts.View()
	case ArticleView:
		return m.readerView()
	default:
		return ""
	}
}
