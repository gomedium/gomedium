package keys

import (
	"github.com/charmbracelet/bubbles/key"
)

type KeyMap struct {
	CursorUp   key.Binding
	CursorDown key.Binding
	Enter      key.Binding
	Back       key.Binding
	Quit       key.Binding
	ForceQuit  key.Binding

	State string
}

func (k KeyMap) ShortHelp() []key.Binding {
	var kb []key.Binding

	if k.State != "browsing" {
		kb = append(kb, k.Enter, k.Back, k.Quit)
	}
	return kb
}

func (k KeyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{}
}

func NewKeyMap() *KeyMap {
	return &KeyMap{
		//CursorUp: key.NewBinding(
		//	key.WithKeys("up"),
		//	key.WithHelp("↑", "move up"),
		//),
		//CursorDown: key.NewBinding(
		//	key.WithKeys("down"),
		//	key.WithHelp("↓", "move down"),
		//),
		Enter: key.NewBinding(
			key.WithKeys("enter"),
			key.WithHelp("enter/<space>", "read article"),
		),
		Back: key.NewBinding(
			key.WithKeys("backspace"),
			key.WithHelp("←/back", "go back"),
		),
		Quit: key.NewBinding(
			key.WithKeys("q"),
			key.WithHelp("q", "quit"),
		),
		ForceQuit: key.NewBinding(
			key.WithKeys("ctrl+c"),
			key.WithHelp("ctrl+c", "force quit"),
		),
	}
}
