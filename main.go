package main

import (
	"fmt"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/rs/zerolog"
	"gitlab.com/gomedium/gomedium/tui"
	"os"
)

func main() {
	zerolog.SetGlobalLevel(zerolog.FatalLevel)
	p := tea.NewProgram(tui.NewModel(), tea.WithAltScreen(), tea.WithMouseCellMotion())
	if err := p.Start(); err != nil {
		fmt.Printf("Alas, there's been an error: %v", err)
		os.Exit(1)
	}
}
