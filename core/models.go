package core

import (
	"encoding/xml"
	"fmt"
	"golang.org/x/exp/utf8string"
	"strings"
)

type PublicationMetadata struct {
	FollowerCount int `json:"followerCount"`
	ActiveAt      int `json:"activeAt"`
}

type Publication struct {
	ID               string              `json:"id"`
	Slug             string              `json:"slug"`
	Name             string              `json:"name"`
	Description      string              `json:"description"`
	ShortDescription string              `json:"shortDescription"`
	ImageID          string              `json:"imageId"`
	HomeURL          string              `json:"homeUrl"`
	FeedURL          string              `json:"feedUrl"`
	MetaData         PublicationMetadata `json:"metadata"`
	Type             string              `json:"type"`
}
type ExportFormat string

const (
	HTML ExportFormat = "HTML"
	MD                = "Markdown"
)

type PostContentType string
type PostMarkupType string

const (
	H3      PostContentType = "H3"            // Heading, title
	H4                      = "H4"            // Subheading
	IMG                     = "IMG"           // Image
	P                       = "P"             // Paragraph
	PQ                      = "PQ"            // Quote (center)
	BQ                      = "BQ"            // Quote (left)
	ULI                     = "ULI"           // List
	PRE                     = "PRE"           // code
	MIXTAPE                 = "MIXTAPE_EMBED" // mixed tape embed external links
	IFRAME                  = "IFRAME"        // GitHub gists, Giphy gifs
)

const (
	A      PostMarkupType = "A"
	EM                    = "EM"
	STRONG                = "STRONG"
	CODE                  = "CODE"
)

type PostMarkup struct {
	Start int            `json:"start"`
	End   int            `json:"end"`
	HRef  string         `json:"href"`
	Type  PostMarkupType `json:"type"`
}

type MixtapeMetadata struct {
	HRef             string `json:"href"`
	ThumbnailImageID string `json:"thumbnailImageId"`
}
type IFrameMetadata struct {
	MediaID string `json:"mediaId"`
	Title   string `json:"title"`
	RootURL string `json:"rootUrl"`
}

type PostContent struct {
	ID              string          `json:"id"`
	Text            string          `json:"text"`
	MediaID         string          `json:"mediaId"`
	Markups         []PostMarkup    `json:"markups"`
	Type            PostContentType `json:"type"`
	MixtapeMetadata MixtapeMetadata `json:"mixtapeMetadata"`
	IFrameMetadata  IFrameMetadata  `json:"iframeMetadata"`
}

func ImageToURL(img string) string {
	return fmt.Sprintf("https://miro.medium.com/max/600/%s", img)
}

func sanatizeURL(u string) string {
	queryIdx := strings.Index(u, "?")
	if queryIdx != -1 {
		return u[:queryIdx]
	}
	return u
}
func HTMLMarkup(s string, markup PostMarkup) string {
	switch markup.Type {
	case A:

		return fmt.Sprintf("<a class=\"medium-markup-a\" href=\"%s\">%s</a>", sanatizeURL(markup.HRef), s)
	case STRONG:
		return fmt.Sprintf("<b>%s</b>", s)
	case EM:
		return fmt.Sprintf("<i>%s</i>", s)
	case CODE:
		return fmt.Sprintf("<code>%s</code>", s)
	default:
		return s
	}
}

func MDMarkup(s string, markup PostMarkup) string {
	switch markup.Type {
	case A:
		return fmt.Sprintf("[%s](%s)", s, sanatizeURL(markup.HRef))
	case STRONG:
		return fmt.Sprintf("**%s**", s)
	case EM:
		return fmt.Sprintf("*%s*", s)
	case CODE:
		return fmt.Sprintf("`%s`", s)
	default:
		return s
	}
}

func (p *PostContent) AddPostMarkup(format ExportFormat) string {
	if len(p.Markups) == 0 {
		return p.Text
	}
	txt := utf8string.NewString(p.Text)
	idx := 0
	res := ""
	var fn func(string, PostMarkup) string
	if format == HTML {
		fn = HTMLMarkup
	} else {
		fn = MDMarkup
	}
	for _, markup := range p.Markups {
		if markup.Start <= idx || markup.Start > txt.RuneCount() || markup.End > txt.RuneCount() {
			continue
		}
		res = txt.Slice(idx, markup.Start) // p.Text[idx:markup.Start]
		markedup := fn(txt.Slice(markup.Start, markup.End), markup)
		res += markedup
		idx = markup.End
	}
	if idx < txt.RuneCount() {
		res = res + txt.Slice(idx, txt.RuneCount())
	}
	return res
}

func (p *PostContent) HTML() string {
	markedup := p.AddPostMarkup(HTML)
	switch p.Type {
	case P:
		return fmt.Sprintf("<p class=\"medium-p\"> %s <p>", markedup)
	case IMG:
		return fmt.Sprintf("<img src=\"%s\" class=\"medium-img\">", ImageToURL(p.MediaID))
	case H3:
		if strings.HasSuffix(p.ID, "_0") {
			return fmt.Sprintf("<h1 class=\"medium-h3 medium-title\">%s </h1>", p.Text)
		}
		return fmt.Sprintf("<h3 class=\"medium-h3 medium-subsection\">%s </h3>", p.Text)
	case H4:
		return fmt.Sprintf("<h4 class=\"medium-h4 medium-subtitle\">%s </h4>", p.Text)
	case PRE:
		return fmt.Sprintf("<div class=\"medium-pre\"> <pre>%s</pre> </div>", p.Text)
	case BQ:
		return fmt.Sprintf("<blockquote class=\"medium-bq\"> <p class=\"medium-bq-inner\">%s</p> </blockquote>", markedup)
	case PQ:
		return fmt.Sprintf("<blockquote class=\"medium-pq\"> <p class=\"medium-pq-inner\">%s</p> </blockquote>", markedup)
	case ULI:
		return fmt.Sprintf("<li class=\"medium-uli\">%s</li>", markedup)
	case IFRAME:
		return fmt.Sprintf("<iframe is=\"x-frame-bypass\" class=\"medium-iframe\" src=\"%s\"> </iframe>", p.IFrameMetadata.RootURL+"/media/"+p.IFrameMetadata.MediaID)
	case MIXTAPE:
		return fmt.Sprintf(`<div class="medium-mixtape"> <a class="medium-mixtape-a" href="%s"> 
									<div class="medium-mixtape-content"> %s </div>
									<div class="medium-mixtape-thumbnail"> %s <div>
									</a> </div>`, p.MixtapeMetadata.HRef, markedup, ImageToURL(p.MixtapeMetadata.ThumbnailImageID))
	default:
		return fmt.Sprintf("<div class=\"medium-unknown\"> %s </div>", p.Text)
	}
}

func (p *PostContent) Markdown() string {
	markedup := p.AddPostMarkup(MD)
	switch p.Type {
	case P:
		return fmt.Sprintf("%s\n", markedup)
	case IMG:
		return fmt.Sprintf("![Image](%s)", ImageToURL(p.MediaID))
	case H3:
		if strings.HasSuffix(p.ID, "_0") {
			//return fmt.Sprintf("# %s", p.Text)
			return ""
		}
		return fmt.Sprintf("### %s", p.Text)
	case H4:
		return fmt.Sprintf("\n\n\n#### %s", p.Text)
	case PRE:
		return fmt.Sprintf("```\n%s\n```", p.Text)
	case BQ:
		return fmt.Sprintf("> %s", markedup)
	case PQ:
		return fmt.Sprintf("> %s", markedup)
	case IFRAME:
		mediaURL := p.IFrameMetadata.RootURL + "/media/" + p.IFrameMetadata.MediaID
		return fmt.Sprintf("> (iframe) **%s%% >> [%s](%s)", p.IFrameMetadata.Title, mediaURL, mediaURL)
	case ULI:
		return fmt.Sprintf("* %s", markedup)
	case MIXTAPE:
		return fmt.Sprintf("> [%s](%s)", p.Text, p.MixtapeMetadata.HRef)
	default:
		return fmt.Sprintf("%s", p.Text)
	}
}

type Post struct {
	ID            string        `json:"id"`
	Title         string        `json:"title"`
	Subtitle      string        `json:"subtitle"`
	Publication   Publication   `json:"publication"`
	URL           string        `json:"url"`
	ClapCount     int           `json:"clapCount"`
	ResponseCount int           `json:"responseCount"`
	Topic         string        `json:"topic"`
	Tags          []string      `json:"tags"`
	ReadingTime   float64       `json:"readingTime"`
	ImageID       string        `json:"imageId"`
	Image         string        `json:"image"`
	Timestamp     int64         `json:"timestamp"`
	Content       []PostContent `json:"content"`
}

func (p *Post) HTML() string {
	template := `<!DOCTYPE html><html>
				 <head>
                 <title>%s</title>
                 <script src="https://unpkg.com/@ungap/custom-elements-builtin"></script>
                 <script type="module" src="https://unpkg.com/x-frame-bypass"></script>
				 </head>
                 <body> %s </body>
				 </html>`
	var elements []string
	for _, c := range p.Content {
		elements = append(elements, c.HTML())
	}
	return fmt.Sprintf(template, p.Title, strings.Join(elements, "\n"))
}

func (p *Post) Markdown() string {
	var elements []string
	for _, c := range p.Content {
		elements = append(elements, c.Markdown())
	}
	return strings.Join(elements, "\n")
}

type RSS struct {
	XMLName xml.Name   `xml:"rss"`
	Channel RSSChannel `xml:"channel"`
}

type RSSChannel struct {
	Title       string    `xml:"title"`
	Link        string    `xml:"RSSDefault link"`
	Description string    `xml:"description"`
	Items       []RSSItem `xml:"item"`
	ID          string
}

type RSSItem struct {
	ID         string
	Title      string   `xml:"title"`
	Guid       string   `xml:"guid"`
	Categories []string `xml:"category"`
	PubDate    string   `xml:"pubDate"`
	Link       string   `xml:"link"`
	Content    string   `xml:"content"`
}
