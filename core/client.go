package core

import (
	"bytes"
	"context"
	"errors"
	"io"
	"net/http"
	"time"
)

type HTTPResult struct {
	Error    error          `json:"error"`
	Response *http.Response `json:"response"`
	Body     string         `json:"body"`
	CacheHit bool           `json:"cacheHit"`
}

func HTTPCall(ctx context.Context, URL, method string, body []byte, timeout int64, headers, cookies map[string]string) HTTPResult {
	logger := ContextLogger(ctx, "client.HTTPCall").With().Str("url", URL).Str("method", method).Int64("timeout", timeout).Logger()
	logger.Info().Msg("start")
	defer logger.Info().Msg("return")

	if method != http.MethodGet && method != http.MethodPost && method != http.MethodPut && method != http.MethodDelete {
		logger.Error().Msg("Invalid method")
		return HTTPResult{Error: errors.New("bad HTTP method"), Response: nil}
	}
	client := &http.Client{
		Timeout: time.Second * time.Duration(timeout),
	}
	// create request
	req, err := http.NewRequest(method, URL, bytes.NewReader(body))
	req.Close = true
	if err != nil {
		logger.Error().Err(err).Msg("failed to create request.")
		return HTTPResult{Error: err, Response: nil}
	}
	// set headers
	for h, hv := range headers {
		req.Header.Set(h, hv)
	}
	//set cookies
	for c, cv := range cookies {
		req.AddCookie(&http.Cookie{Name: c, Value: cv})
	}
	// send request
	res, err := client.Do(req)
	if err != nil {
		logger.Error().Err(err).Msg("failed to do request.")
		return HTTPResult{Error: err, Response: nil}
	}
	// validate response
	if res.StatusCode != http.StatusOK {
		logger.Error().Msg("Unsuccessful request with status code " + res.Status)
		return HTTPResult{Error: errors.New("response " + res.Status)}
	}

	logger.Info().Msg("Successful request.")
	data, err := io.ReadAll(res.Body)
	defer res.Body.Close()
	if err != nil {
		logger.Error().Err(err).Msg("error reading body")
	}

	out := HTTPResult{Error: nil, Response: res, Body: string(data)}
	return out
}

func DefaultUserAgent() map[string]string {
	return map[string]string{
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
		"Referer":    "https://t.co/",
	}
}
