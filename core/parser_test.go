package core

import (
	"context"
	"math"
	"strings"
	"testing"
)

type RSSTestExpect struct {
	err         bool
	chanID      string
	chanTitle   string
	firstPostID string
}
type RSSTestCase struct {
	inp    string
	expect RSSTestExpect
}

type PairTestCase struct {
	inp    string
	expect any
}

const MediumRSS = `<?xml version="1.0" encoding="UTF-8"?><rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0" xmlns:cc="http://cyber.law.harvard.edu/rss/creativeCommonsRssModule.html">
    <channel>
        <title><![CDATA[The Startup - Medium]]></title>
        <description><![CDATA[Get smarter at building your thing. Follow to join The Startup’s +8 million monthly readers &amp; +756K followers. - Medium]]></description>
        <link>https://medium.com/swlh?source=rss----f5af2b715248---4</link>
        <image>
            <url>https://cdn-images-1.medium.com/proxy/1*TGH72Nnw24QL3iV9IOm4VA.png</url>
            <title>The Startup - Medium</title>
            <link>https://medium.com/swlh?source=rss----f5af2b715248---4</link>
        </image>
        <generator>Medium</generator>
        <lastBuildDate>Thu, 23 Jun 2022 03:40:20 GMT</lastBuildDate>
        <atom:link href="https://medium.com/feed/swlh" rel="self" type="application/rss+xml"/>
        <webMaster><![CDATA[yourfriends@medium.com]]></webMaster>
        <atom:link href="http://medium.superfeedr.com" rel="hub"/>
        <item>
            <title><![CDATA[I Make 3-Figure Sales Monthly From My New Site— Here Is How I Do It]]></title>
            <description><![CDATA[<div class="medium-feed-item"><p class="medium-feed-image"><a href="https://medium.com/swlh/i-make-3-figure-sales-monthly-from-my-new-site-here-is-how-i-do-it-bdcfb5fe5b46?source=rss----f5af2b715248---4"><img src="https://cdn-images-1.medium.com/max/768/1*SSJjWLTruWg0dsD52kplgQ.jpeg" width="768"></a></p><p class="medium-feed-snippet">My site&#x2019;s growth strategy, mindset, and tips to create content that hits Google&#x2019;s front page.</p><p class="medium-feed-link"><a href="https://medium.com/swlh/i-make-3-figure-sales-monthly-from-my-new-site-here-is-how-i-do-it-bdcfb5fe5b46?source=rss----f5af2b715248---4">Continue reading on The Startup »</a></p></div>]]></description>
            <link>https://medium.com/swlh/i-make-3-figure-sales-monthly-from-my-new-site-here-is-how-i-do-it-bdcfb5fe5b46?source=rss----f5af2b715248---4</link>
            <guid isPermaLink="false">https://medium.com/p/bdcfb5fe5b46</guid>
            <category><![CDATA[side-hustle]]></category>
            <category><![CDATA[business]]></category>
            <category><![CDATA[creativity]]></category>
            <category><![CDATA[entrepreneurship]]></category>
            <category><![CDATA[seo]]></category>
            <dc:creator><![CDATA[Victoria Kurichenko]]></dc:creator>
            <pubDate>Wed, 22 Jun 2022 19:47:55 GMT</pubDate>
            <atom:updated>2022-06-22T19:47:54.812Z</atom:updated>
        </item>
        <item>
            <title><![CDATA[Ten Sentences About Money That Saved Me From Spending $80,000 on a Finance Degree]]></title>
            <description><![CDATA[<div class="medium-feed-item"><p class="medium-feed-image"><a href="https://medium.com/swlh/ten-sentences-about-money-that-saved-me-from-spending-80-000-on-a-finance-degree-6dea6f3c3dd3?source=rss----f5af2b715248---4"><img src="https://cdn-images-1.medium.com/max/2600/1*fMWkR-P9ccyWskeh0B5_DA.jpeg" width="4912"></a></p><p class="medium-feed-snippet">#1. Time is the real currency</p><p class="medium-feed-link"><a href="https://medium.com/swlh/ten-sentences-about-money-that-saved-me-from-spending-80-000-on-a-finance-degree-6dea6f3c3dd3?source=rss----f5af2b715248---4">Continue reading on The Startup »</a></p></div>]]></description>
            <link>https://medium.com/swlh/ten-sentences-about-money-that-saved-me-from-spending-80-000-on-a-finance-degree-6dea6f3c3dd3?source=rss----f5af2b715248---4</link>
            <guid isPermaLink="false">https://medium.com/p/6dea6f3c3dd3</guid>
            <category><![CDATA[self-improvement]]></category>
            <category><![CDATA[entrepreneurship]]></category>
            <category><![CDATA[business]]></category>
            <category><![CDATA[money]]></category>
            <category><![CDATA[finance]]></category>
            <dc:creator><![CDATA[Darshak Rana]]></dc:creator>
            <pubDate>Wed, 22 Jun 2022 17:46:09 GMT</pubDate>
            <atom:updated>2022-06-22T17:46:08.759Z</atom:updated>
        </item>
    </channel>
</rss>`

func TestParseFeed(t *testing.T) {
	// TODO add more tests
	var tests [1]RSSTestCase
	tests[0] = RSSTestCase{
		inp: MediumRSS,
		expect: RSSTestExpect{
			err:         false,
			chanID:      "f5af2b715248",
			chanTitle:   "The Startup",
			firstPostID: "bdcfb5fe5b46",
		},
	}
	ctx := context.WithValue(context.TODO(), "trackID", "track-123")
	for _, test := range tests {
		feed, err := GetRSSFeed(ctx, test.inp)
		if (err == nil) == test.expect.err {
			t.Fail()
		}
		if !strings.Contains(feed.Channel.Title, test.expect.chanTitle) {
			t.Error("wrong channel title")
		}
		if feed.Channel.ID != test.expect.chanID {
			t.Error("wrong channel ID")
		}
		if feed.Channel.Items[0].ID != test.expect.firstPostID {
			t.Error("wrong post ID")
		}
	}
}

func TestJSONProfile(t *testing.T) {
	var tests [8]PairTestCase
	tests[0] = PairTestCase{"https://medium.com/@bradwray/feed", "https://medium.com/@bradwray?format=json"}
	tests[1] = PairTestCase{"https://medium.com/@bradwray/", "https://medium.com/@bradwray?format=json"}
	tests[2] = PairTestCase{"https://cajundiscordian.medium.com", "https://medium.com/@cajundiscordian?format=json"}
	tests[3] = PairTestCase{"https://medium.com/swlh", "https://medium.com/swlh?format=json"}
	tests[4] = PairTestCase{"https://towardsdatascience.com", "https://towardsdatascience.com?format=json"}
	tests[5] = PairTestCase{"https://medium.com/@bradwray/ive-had-enough-of-teaching-2bae1a660832", "https://medium.com/@bradwray?format=json"}
	tests[6] = PairTestCase{"https://towardsdatascience.com/an-intuitive-explanation-of-sentence-bert-1984d144a868", "https://towardsdatascience.com?format=json"}
	tests[7] = PairTestCase{"https://medium.com/swlh/i-make-3-figure-sales-monthly-from-my-new-site-here-is-how-i-do-it-bdcfb5fe5b46", "https://medium.com/swlh?format=json"}
	for _, test := range tests {
		u, err := JSONProfileURL(test.inp)
		res := HTTPCall(context.TODO(), u, "GET", nil, 10, nil, nil)
		if err != nil {
			t.Error(err)
		}
		if u != test.expect {
			t.Errorf("expected %s but got %s", test.expect, u)
		}
		if res.Error != nil {
			t.Error(res.Error)
		}
	}
}

func TestPostIDFromURL(t *testing.T) {
	var tests [6]PairTestCase
	tests[0] = PairTestCase{"https://medium.com/@bradwray/ive-had-enough-of-teaching-2bae1a660832/", "2bae1a660832"}
	tests[1] = PairTestCase{"https://medium.com/@bradwray/ive-had-enough-of-teaching-2bae1a660832", "2bae1a660832"}
	tests[2] = PairTestCase{"https://towardsdatascience.com/an-intuitive-explanation-of-sentence-bert-1984d144a868", "1984d144a868"}
	tests[3] = PairTestCase{"https://towardsdatascience.com/an-intuitive-explanation-of-sentence-bert-1984d144a868/", "1984d144a868"}
	tests[4] = PairTestCase{"https://medium.com/swlh/i-make-3-figure-sales-monthly-from-my-new-site-here-is-how-i-do-it-bdcfb5fe5b46", "bdcfb5fe5b46"}
	tests[5] = PairTestCase{"https://cajundiscordian.medium.com/scientific-data-and-religious-opinions-ff9b0938fc10", "ff9b0938fc10"}

	for _, test := range tests {
		u, err := PostIDFromURL(test.inp)
		if err != nil {
			t.Error(err)
		}
		if u != test.expect {
			t.Errorf("input %s => expected %s but got %s", test.inp, test.expect, u)
		}
	}
}

func TestGetPost(t *testing.T) {
	URL := "https://medium.com/@bradwray/ive-had-enough-of-teaching-2bae1a660832/"
	post, err := GetPost(context.TODO(), URL)
	if err != nil {
		t.Error(err)
	}
	if post.ID != "2bae1a660832" {
		t.Error("wrong post ID")
	}
	if post.Title != "I’ve had enough of teaching" {
		t.Error("wrong post title")
	}
	if len(post.Content) == 0 {
		t.Error("no content")
	}
	markdown := post.Markdown()
	html := post.HTML()
	if !strings.Contains(markdown, "After 14 years as a high school teacher, I’ve decided to move on") {
		t.Error("bad/wrong markdown content")
	}
	if !strings.Contains(html, "students in the year 2022 are incompatible ") {
		t.Error("bad/wrong html output")
	}
	if post.Publication.Type != "user" {
		t.Error("wrong pub type")
	}
	if post.Publication.Slug != "@bradwray" {
		t.Error("wrong pub slug")
	}
	if post.Publication.Name != "Brad Wray" {
		t.Error("wrong pub name")
	}

	u := "https://towardsdatascience.com/an-intuitive-explanation-of-sentence-bert-1984d144a868"
	post, err = GetPost(context.TODO(), u)
	if err != nil {
		t.Error(err)
	}
	if post.ID != "1984d144a868" {
		t.Error("wrong post ID")
	}
	if post.Title != "An Intuitive Explanation of Sentence-BERT" {
		t.Error("wrong post title")
	}
	if len(post.Content) == 0 {
		t.Error("no content")
	}
	markdown = post.Markdown()
	html = post.HTML()
	if !strings.Contains(markdown, "from sentence_transformers import SentenceTransformer") {
		t.Error("bad/wrong markdown content")
	}
	if !strings.Contains(html, "One reason why Siamese networks are so powerful") {
		t.Error("bad/wrong html output")
	}
	if post.Publication.Type != "collection" {
		t.Error("wrong pub type")
	}
	if post.Publication.Slug != "towards-data-science" {
		t.Error("wrong pub slug")
	}
	if math.Abs(post.ReadingTime-6.8) > 1.0 {
		t.Error("wrong reading time")
	}
	if post.Publication.Name != "Towards Data Science" {
		t.Error("wrong pub name")
	}

	URL = "https://towardsdatascience.com/an-intuitive-explanation-of-sentence-bert"
	_, err = GetPost(context.TODO(), URL)
	if err != nil {
		t.Fail()
	}

}

func TestPublicationHome(t *testing.T) {
	url := "https://medium.com/feed/swlh"
	pub, err := PublicationHome(context.TODO(), url)
	if err != nil {
		t.Error(err)
	}
	if pub.Name != "The Startup" || pub.ID != "f5af2b715248" {
		t.Error("wrong name/id")
	}
	if pub.Type != "collection" {
		t.Error("wrong publication type")
	}
	if pub.FeedURL != "https://medium.com/feed/swlh" {
		t.Error("wrong feed URL")
	}

	posts, err := PublicationPosts(context.TODO(), pub)
	if err != nil {
		t.Error(err)
	}
	if len(posts) < 1 {
		t.Error("no posts extracted")
	}

	url = "https://towardsdatascience.com"
	pub, err = PublicationHome(context.TODO(), url)
	if err != nil {
		t.Error(err)
	}
	if pub.Name != "Towards Data Science" {
		t.Error("wrong pub name")
	}
	if pub.FeedURL != "https://towardsdatascience.com/feed" {
		t.Error("wrong feed URL")
	}
	posts, err = PublicationPosts(context.TODO(), pub)
	if err != nil {
		t.Error(err)
	}
	if len(posts) == 0 {
		t.Error("no posts extracted")
	}

	url = "https://medium.com/@bradwray"
	pub, err = PublicationHome(context.TODO(), url)
	if err != nil {
		t.Error(err)
	}
	if pub.Name != "Brad Wray" {
		t.Error("wrong name")
	}
	if pub.FeedURL != "https://medium.com/feed/@bradwray" {
		t.Error("wrong feed URL")
	}

	posts, err = PublicationPosts(context.TODO(), pub)
	if err != nil {
		t.Error(err)
	}
	if len(posts) == 0 {
		t.Error("no posts extracted")
	}
}
