package core

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"fmt"
	"net/http"
	nurl "net/url"
	"regexp"
	"strings"
	"sync"

	"github.com/tidwall/gjson"
)

func IsCustomMediumDomain(URL string) bool {
	return !strings.Contains(URL, "medium.com")
}

func IsRSSFeedURL(URL string) bool {
	return strings.Contains(URL, "/feed")
}

func JSONProfileURL(URL string) (string, error) {
	if !strings.HasPrefix(URL, "https://") {
		URL = "https://" + URL
	}
	if IsRSSFeedURL(URL) {
		URL = strings.Replace(URL, "/feed", "", 1)
	}
	parsed, err := nurl.Parse(URL)
	jsonQuery, _ := nurl.Parse("?format=json")
	if err != nil {
		return "", err
	}
	if IsCustomMediumDomain(URL) {
		parsed.Path = ""
		parsed.RawQuery = ""
		return parsed.ResolveReference(jsonQuery).String(), nil
	}

	var username string
	paths := strings.Split(strings.TrimPrefix(parsed.Path, "/"), "/")
	if parsed.Host != "medium.com" {
		username = "@" + strings.Split(parsed.Host, ".")[0]
	} else {
		username = paths[0]
	}
	resBase, _ := nurl.Parse(fmt.Sprintf("https://medium.com/%s", username))
	return resBase.ResolveReference(jsonQuery).String(), nil
}

func RSSAddress(URL string) (string, error) {
	if !strings.HasPrefix(URL, "https://") {
		URL = "https://" + URL
	}
	if IsRSSFeedURL(URL) {
		URL = strings.Replace(URL, "/feed", "", 1)
	}
	parsed, err := nurl.Parse(URL)
	if err != nil {
		return "", err
	}
	if IsCustomMediumDomain(URL) {
		return fmt.Sprintf("https://%s/feed", parsed.Host), nil
	}

	var username string
	paths := strings.Split(strings.TrimPrefix(parsed.Path, "/"), "/")
	if parsed.Host != "medium.com" {
		return fmt.Sprintf("https://%s/feed", parsed.Host), nil
	} else {
		username = paths[0]
		return fmt.Sprintf("https://medium.com/feed/%s", username), nil
	}
}

func PublicationHome(ctx context.Context, URL string) (Publication, error) {
	logger := ContextLogger(ctx, "parser.PublicationHome").With().Str("URL", URL).Logger()
	defer logger.Info().Msg("return")
	logger.Info().Msg("start")
	profile, err := JSONProfileURL(URL)
	if err != nil {
		logger.Error().Err(err).Msg("error getting json profile address")
		return Publication{}, err
	}
	res := HTTPCall(ctx, profile, "GET", nil, 15, DefaultUserAgent(), nil)
	if res.Error != nil {
		logger.Error().Err(err).Msg("error calling json profile address")
	}
	body := strings.TrimSuffix(res.Body, "])}while(1);</x>")
	collection := gjson.Get(body, "payload.collection")
	RSSLink, err := RSSAddress(URL)
	if err != nil {
		logger.Error().Err(err).Msg("couldn't get RSS address")
		return Publication{}, err
	}
	if collection.Exists() {
		pub := Publication{
			ID:               collection.Get("id").Str,
			Name:             collection.Get("name").Str,
			Slug:             collection.Get("slug").Str,
			Description:      collection.Get("description").Str,
			ShortDescription: collection.Get("shortDescription").Str,
			HomeURL:          profile[:len(profile)-len("?format=json")],
			Type:             "collection",
			FeedURL:          RSSLink,
			MetaData: PublicationMetadata{
				FollowerCount: int(collection.Get("metadata.followerCount").Num),
				ActiveAt:      int(collection.Get("metadata.activeAt").Num),
			},
			ImageID: collection.Get("image.imageId").Str,
		}
		return pub, nil
	}

	user := gjson.Get(body, "payload.user")
	if !user.Exists() {
		err = errors.New("payload has neither user nor collection")
		logger.Error().Err(err).Msg("weird payload")
		return Publication{}, err
	}
	uid := user.Get("userId").Str
	pub := Publication{
		ID:               uid,
		Name:             user.Get("name").Str,
		Slug:             "@" + user.Get("username").Str,
		HomeURL:          "https://medium.com/@" + user.Get("username").Str,
		FeedURL:          RSSLink,
		Description:      user.Get("bio").Str,
		ShortDescription: user.Get("bio").Str,
		ImageID:          user.Get("imageId").Str,
		Type:             "user",
		MetaData:         PublicationMetadata{FollowerCount: int(user.Get("references.SocialStats" + uid).Get("usersFollowedByCount").Int())},
	}
	return pub, nil
}

func ParallelGet(ctx context.Context, pids []string) map[string]Post {
	type getResult struct {
		res Post
		ID  string
	}
	select {
	case <-ctx.Done():
		return nil
	default:
		var wg sync.WaitGroup
		resChannel := make(chan getResult, len(pids))
		defer close(resChannel)
		for _, i := range pids {
			wg.Add(1)
			go func(pid string) {
				defer wg.Done()
				p, err := GetPostById(ctx, pid)
				if err == nil {
					resChannel <- getResult{p, pid}
				}
			}(i)
		}
		wg.Wait()
		// drain the channel
		output := make(map[string]Post)
		for len(resChannel) > 0 {
			r := <-resChannel
			output[r.ID] = r.res
		}
		return output
	}
}
func PublicationPosts(ctx context.Context, pub Publication) (posts []Post, err error) {
	logger := ContextLogger(ctx, "parser.PublicationPosts").With().Str("pubRSS", pub.FeedURL).Logger()
	res := HTTPCall(ctx, pub.FeedURL, http.MethodGet, nil, 15, DefaultUserAgent(), nil)
	if res.Error != nil {
		return posts, res.Error
	}
	rss, err := GetRSSFeed(ctx, res.Body)
	if err != nil {
		return
	}

	var pids []string
	for _, item := range rss.Channel.Items {
		pids = append(pids, item.ID)
	}
	fetchedPosts := ParallelGet(ctx, pids)
	for _, item := range rss.Channel.Items {
		post, okay := fetchedPosts[item.ID]
		if okay {
			posts = append(posts, post)
		} else {
			logger.Error().Str("postID", item.ID).Msg("couldn't get this post by id")
		}
	}
	return posts, nil
}

func PostIDFromURL(URL string) (string, error) {
	URL = strings.TrimSuffix(URL, "/")
	if !strings.Contains(URL, "-") {
		return "", errors.New("valid medium posts have *-[postID] pattern")
	}
	parsed, err := nurl.Parse(URL)
	if err != nil {
		return "", err
	}
	paths := strings.Split(parsed.Path, "-")
	return paths[len(paths)-1], nil
}

func postIDFromPermaLink(URL string) string {
	prefix := "https://medium.com/p/"
	if strings.HasPrefix(URL, prefix) {
		return URL[len(prefix):]
	}
	prefix = "medium.com/p/"
	start := strings.Index(URL, prefix)
	if start != -1 && len(prefix)+start < len(URL) {
		return URL[start+len(prefix):]
	}
	return ""
}

func PublisherID(URL string) string {
	r, _ := regexp.Compile(".*\\?source=rss-+([a-zA-Z\\d]+).*")
	match := r.MatchString(URL)
	if !match {
		return ""
	}
	submatches := r.FindStringSubmatch(URL)
	if len(submatches) < 2 {
		return ""
	}
	return submatches[1]
}

func GetRSSFeed(ctx context.Context, body string) (RSS, error) {
	logger := ContextLogger(ctx, "parser.GetRSSFeed").With().Logger()
	logger.Info().Msg("start")
	defer logger.Info().Msg("return")

	data := []byte(body)
	var rss RSS
	decoder := xml.NewDecoder(bytes.NewReader(data))
	decoder.DefaultSpace = "RSSDefault"
	err := decoder.Decode(&rss)
	if err != nil {
		logger.Error().Err(err).Str("body", body).Msg("couldn't xml unmarshal")
		return rss, err
	}
	pubID := PublisherID(rss.Channel.Link)
	if len(pubID) < 1 {
		logger.Error().Str("RSSChannel.Link", rss.Channel.Link).Msg("regex matching failed")
	}
	rss.Channel.ID = pubID
	for i, _ := range rss.Channel.Items {
		pid := postIDFromPermaLink(rss.Channel.Items[i].Guid)
		if len(pid) == 0 {
			logger.Error().Str("guid", rss.Channel.Items[i].Guid).Msg("couldn't extract post id")
		}
		rss.Channel.Items[i].ID = pid
	}
	return rss, nil
}

func extractPublisher(body string) (Publication, error) {
	data := gjson.Parse(body)
	if len(data.Array()) == 0 {
		return Publication{}, errors.New("wrong data format")
	}
	data = data.Array()[0].Get("data.postResult")
	if !data.Exists() {
		return Publication{}, errors.New("no post in the json")
	}
	isCollection := data.Get("collection").Type != gjson.Null
	var pub gjson.Result
	var res Publication
	if isCollection {
		pub = data.Get("collection")
		res = Publication{
			Type:             "collection",
			ID:               pub.Get("id").Str,
			Name:             pub.Get("name").Str,
			ImageID:          pub.Get("avatar.id").Str,
			Slug:             pub.Get("slug").Str,
			Description:      pub.Get("description").Str,
			ShortDescription: pub.Get("description").Str,
			HomeURL:          fmt.Sprintf("https://%s", pub.Get("domain").Str),
			FeedURL:          fmt.Sprintf("https://%s/feed", pub.Get("domain").Str),
			MetaData: PublicationMetadata{
				FollowerCount: int(pub.Get("subscriberCount").Num),
			},
		}
	} else {
		pub = data.Get("creator")
		res = Publication{
			Type:             "user",
			ID:               pub.Get("id").Str,
			Name:             pub.Get("name").Str,
			ImageID:          pub.Get("imageId").Str,
			Slug:             "@" + pub.Get("username").Str,
			Description:      pub.Get("bio").Str,
			ShortDescription: pub.Get("bio").Str,
			HomeURL:          fmt.Sprintf("https://medium.com/@%s", pub.Get("username").Str),
			FeedURL:          fmt.Sprintf("https://medium.com/feed/@%s", pub.Get("username").Str),
			MetaData: PublicationMetadata{
				FollowerCount: int(pub.Get("socialStats.followerCount").Num),
			},
		}
	}

	return res, nil
}

func parseParagraph(ctx context.Context, post Post, data gjson.Result) PostContent {
	content := PostContent{
		ID:      data.Get("id").Str,
		Type:    PostContentType(data.Get("type").Str),
		Text:    data.Get("text").Str,
		MediaID: data.Get("metadata.id").Str,
		MixtapeMetadata: MixtapeMetadata{
			HRef:             data.Get("mixtapeMetadata.href").Str,
			ThumbnailImageID: data.Get("mixtapeMetadata.thumbnailImageId").Str,
		},
		IFrameMetadata: IFrameMetadata{
			MediaID: data.Get("iframe.mediaResource.id").Str,
			Title:   data.Get("iframe.mediaResource.title").Str,
			RootURL: fmt.Sprintf("https://%s", Host(post.Publication.HomeURL)),
		},
	}

	// markups
	for _, markup := range data.Get("markups").Array() {
		content.Markups = append(content.Markups, PostMarkup{
			Type:  PostMarkupType(markup.Get("type").String()),
			Start: int(markup.Get("start").Int()),
			End:   int(markup.Get("end").Int()),
			HRef:  markup.Get("href").Str,
		})
	}
	return content
}

func ExtractContent(ctx context.Context, post Post, data gjson.Result) []PostContent {
	paragraphs := data.Get("paragraphs").Array()
	var res []PostContent
	for _, par := range paragraphs {
		parsed := parseParagraph(ctx, post, par)
		res = append(res, parsed)
	}
	return res
}

func makePost(ctx context.Context, body string) (Post, error) {
	logger := ContextLogger(ctx, "parser.makePost")
	logger.Info().Msg("start")
	defer logger.Info().Msg("done")

	pub, err := extractPublisher(body)

	if err != nil {
		return Post{}, err
	}
	data := gjson.Parse(body)
	data = data.Array()[0].Get("data.postResult")

	post := Post{
		ID:            data.Get("id").Str,
		URL:           data.Get("mediumUrl").Str,
		Title:         data.Get("title").Str,
		Subtitle:      data.Get("previewContent.subtitle").Str,
		Publication:   pub,
		ImageID:       data.Get("previewImage.id").Str,
		Image:         fmt.Sprintf("https://miro.medium.com/max/400/%s", data.Get("previewImage.id").Str),
		ClapCount:     int(data.Get("clapCount").Int()),
		ResponseCount: int(data.Get("postResponses.count").Int()),
		Timestamp:     data.Get("firstPublishedAt").Int(),
		ReadingTime:   data.Get("readingTime").Float(),
		Topic:         data.Get("topics.1.name").Str,
	}
	// post tags
	tagArray := data.Get("tags.#.id").Array()
	for _, tag := range tagArray {
		post.Tags = append(post.Tags, tag.String())
	}
	post.Content = ExtractContent(ctx, post, data.Get("content.bodyModel"))
	return post, nil
}

func GetPostById(ctx context.Context, postID string) (Post, error) {
	logger := ContextLogger(ctx, "parser.GetPost").With().Str("postID", postID).Logger()
	defer logger.Info().Msg("return")
	logger.Info().Msg("start")
	query := PostViewQuery(postID)
	header := map[string]string{
		"Content-Type": "application/json",
	}
	res := HTTPCall(ctx, MediumAPI, http.MethodPost, []byte(query), 15, header, nil)
	if res.Error != nil {
		logger.Error().Err(res.Error).Msg("error in http call to get post")
		return Post{}, res.Error
	}
	post, err := makePost(context.TODO(), res.Body)
	if err != nil {
		logger.Error().Err(err).Str("resBody", res.Body).Msg("error creating post from response")
		return Post{}, err
	}
	return post, nil
}

func GetPost(ctx context.Context, URL string) (Post, error) {
	logger := ContextLogger(ctx, "parser.GetPost").With().Str("URL", URL).Logger()
	defer logger.Info().Msg("done")
	logger.Info().Msg("start")
	postID, err := PostIDFromURL(URL)
	if err != nil {
		logger.Error().Err(err).Msg("error getting post ID")
		return Post{}, err
	}
	logger.Info().Str("postID", postID).Msg("extracted PostID")
	return GetPostById(ctx, postID)
}
