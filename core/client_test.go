package core

import (
	"context"
	"github.com/tidwall/gjson"
	"net/http"
	"strings"
	"testing"
)

func TestHTTPCall(t *testing.T) {
	url := "https://medium.com/swlh?format=json"
	ctx := context.WithValue(context.TODO(), "trackID", "track-123")
	res := HTTPCall(ctx, url, http.MethodGet, nil, 10, nil, nil)
	bodyStr := res.Body
	bodyStr = strings.TrimPrefix(bodyStr, "])}while(1);</x>")
	collection := gjson.Get(bodyStr, "payload.collection")
	pub := Publication{
		ID:               collection.Get("id").Str,
		Name:             collection.Get("name").Str,
		Slug:             collection.Get("slug").Str,
		Description:      collection.Get("description").Str,
		ShortDescription: collection.Get("shortDescription").Str,
		MetaData: PublicationMetadata{
			FollowerCount: int(collection.Get("metadata.followerCount").Num),
			ActiveAt:      int(collection.Get("metadata.activeAt").Num),
		},
		ImageID: collection.Get("image.imageId").Str,
		//Image:   fmt.Sprintf("https://miro.medium.com/max/400/%s", collection.Get("image.imageId").Str),
	}
	if len(pub.ID) < 1 {
		t.Fail()
	}
}
