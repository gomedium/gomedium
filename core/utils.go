package core

import (
	"context"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	nurl "net/url"
)

func ContextLogger(ctx context.Context, caller string) zerolog.Logger {
	logger := log.With().Str("caller", "gomedium.core"+caller).Logger()
	if trackID, ok := ctx.Value("trackID").(string); ok {
		logger = logger.With().Str("trackID", trackID).Logger()
	}
	return logger
}

func Host(URL string) string {
	u, _ := nurl.Parse(URL)
	return u.Host
}
